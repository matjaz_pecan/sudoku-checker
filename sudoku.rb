#!/usr/bin/env ruby

require 'matrix'


class Sudoku
  @grid_matrix = nil

  class DuplicatedRowValueError < StandardError; end
  class DuplicatedColumnValueError < StandardError; end

  def initialize(grid)
    @grid_matrix = Matrix::rows(grid)
  end

  def set_grid(grid)
    @grid_matrix = Matrix::rows(grid)
  end

  def is_finished
    if !is_grid_valid()
      return false
    end

    if !are_rows_valid()
      return false
    end

    if !are_columns_valid()
      return false
    end

    if !are_regions_valid()
      return false
    end

    return true
  end

  private

  def is_grid_valid
    # check number of rows
    if @grid_matrix.row_count != 9
      raise ArgumentError, 'Grid must have 9 rows!'
    end

    # check number of rows
    if @grid_matrix.column_count != 9
      raise ArgumentError, 'Grid lines must have 9 columns!'
    end

    # check if values are valid
    (0..8).each do |column_index|
      (0..8).each do |row_index|
        if !@grid_matrix[row_index, column_index].kind_of?(Integer)
          raise ArgumentError, 'Grid values must be Integer!'
        end

        # check if number is in range 1 - 9
        if !(1..9).member?(@grid_matrix[row_index, column_index])
          raise ArgumentError, 'Grid values must be in range 1..9!'
        end
      end
    end

    return true
  end

  def are_rows_valid
    (0..8).each do |index|
      used_values = []

      @grid_matrix.row(index).each do |num|
        if used_values.include?(num)
          raise DuplicatedRowValueError, 'Every number must be unique in specific row!'
        end

        used_values.push(num)
      end
    end

    return true
  end

  def are_columns_valid
    (0..8).each do |index|
      used_values = []

      @grid_matrix.column(index).each do |num|
        if used_values.include?(num)
          raise DuplicatedColumnValueError, 'Every number must be unique in specific column!'
        end

        used_values.push(num)
      end
    end

    return true
  end

  def are_regions_valid
    [0, 3, 6].each do |column_index|
      [0, 3, 6].each do |row_index|
        # start_row, nrows, start_col, ncols
        sub_grid = @grid_matrix.minor(row_index, 3, column_index, 3)

        used_values = []

        sub_grid.each do |num|
          if used_values.include?(num)
            raise DuplicatedColumnValueError, 'Every number must be unique in specific column!'
          end

          used_values.push(num)
        end
      end
    end

    return true
  end
end


def done_or_not(sudoku_grid)
  begin
    sudoku = Sudoku.new(sudoku_grid)

    if sudoku.is_finished()
      return 'Finished!'
    end
  rescue
    # handle error if needed
  end

  return 'Try again!'
end


if __FILE__ == $0
  grid = [
    [8, 3, 4, 9, 7, 6, 5, 1, 2],
    [6, 5, 7, 4, 1, 2, 9, 8, 3],
    [1, 9, 2, 3, 8, 5, 4, 6, 7],

    [4, 6, 3, 1, 2, 9, 7, 5, 8],
    [5, 2, 9, 7, 4, 8, 6, 3, 1],
    [7, 8, 1, 6, 5, 3, 2, 9, 4],

    [3, 7, 5, 8, 9, 4, 1, 2, 6],
    [2, 1, 6, 5, 3, 7, 8, 4, 9],
    [9, 4, 8, 2, 6, 1, 3, 7, 5]
  ]

  puts done_or_not(grid)
end
