#!/usr/bin/env ruby

require_relative "sudoku"
require "test/unit"

$valid_grid = 'Finished!'
$invalid_grid = 'Try again!'

class TestValidSudoku < Test::Unit::TestCase
  def test_valid_nil
    assert_equal($invalid_grid, done_or_not(nil))
  end

  def test_valid_variable
    assert_equal($invalid_grid, done_or_not('a'))
  end

  def test_valid_array
    assert_equal($invalid_grid, done_or_not([1]))
  end

  def test_valid_grid
    arr = [
        [], 1, [], [], [], 'a', [], [], []
    ]

    assert_equal($invalid_grid, done_or_not(arr))
  end

  def test_valid_grid_size
    arr = [
        [], [], [], [], [], [], [], [], [], [], [], []
    ]

    assert_equal($invalid_grid, done_or_not(arr))
  end

  def test_valid_values
    arr = [
        [1, 'a', 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, '3', 4, 5, 6, 7, 8, 9],

        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, [], 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4.23, 5, 6, 7, 8, 9],

        [1, 2, 3, 99, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
    ]

    assert_equal($invalid_grid, done_or_not(arr))
  end

  def test_valid_rows
    arr = [
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],

        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],

        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
    ]

    assert_equal($invalid_grid, done_or_not(arr))
  end

  def test_valid_columns
    arr = [
        [1, 1, 1, 1, 1, 1, 1, 1, 1],
        [2, 2, 2, 2, 2, 2, 2, 2, 2],
        [3, 3, 3, 3, 3, 3, 3, 3, 3],

        [4, 4, 4, 4, 4, 4, 4, 4, 4],
        [5, 5, 5, 5, 5, 5, 5, 5, 5],
        [6, 6, 6, 6, 6, 6, 6, 6, 6],

        [7, 7, 7, 7, 7, 7, 7, 7, 7],
        [8, 8, 8, 8, 8, 8, 8, 8, 8],
        [9, 9, 9, 9, 9, 9, 9, 9, 9],
    ]

    assert_equal($invalid_grid, done_or_not(arr))
  end

  def test_solved_example
    arr = [
        [5, 8, 9, 7, 4, 2, 1, 3, 6],
        [1, 2, 4, 5, 3, 6, 9, 8, 7],
        [7, 6, 3, 1, 8, 9, 5, 2, 4],

        [2, 5, 8, 3, 6, 7, 4, 9, 1],
        [4, 7, 6, 9, 1, 8, 2, 5, 3],
        [3, 9, 1, 2, 5, 4, 7, 6, 8],

        [6, 3, 5, 4, 9, 1, 8, 7, 2],
        [8, 1, 2, 6, 7, 5, 3, 4, 9],
        [9, 4, 7, 8, 2, 3, 6, 1, 5]
    ]

    assert_equal($valid_grid, done_or_not(arr))
  end


  def test_solved_example_column_swap
    arr = [
        [5, 8, 7, 9, 4, 2, 1, 3, 6],
        [1, 2, 5, 4, 3, 6, 9, 8, 7],
        [7, 6, 1, 3, 8, 9, 5, 2, 4],

        [2, 5, 3, 8, 6, 7, 4, 9, 1],
        [4, 7, 9, 6, 1, 8, 2, 5, 3],
        [3, 9, 2, 1, 5, 4, 7, 6, 8],

        [6, 3, 4, 5, 9, 1, 8, 7, 2],
        [8, 1, 6, 2, 7, 5, 3, 4, 9],
        [9, 4, 8, 7, 2, 3, 6, 1, 5]
    ]

    assert_equal($invalid_grid, done_or_not(arr))
  end
end
